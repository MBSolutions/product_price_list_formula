# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.transaction import Transaction
from trytond.pool import Pool, PoolMeta

from decimal import Decimal

__all__ = ['PriceList', 'PriceListLine']
__metaclass__ = PoolMeta


class PriceList:
    __name__ = 'product.price_list'

    @classmethod
    def __setup__(cls):
        super(PriceList, cls).__setup__()
        cls._error_messages.update({
            'not_found_price_list': 'Could not find price list: %s!',
        })

    def _get_context_price_list_line(self, party, product, unit_price,
            quantity, uom):
        '''
        Add party, product, quantity, uom and price_list to be
        available in formulas

        :param party: the BrowseRecord of the party.party
        :param product: the BrowseRecord of the product.product
        :param unit_price: a Decimal for the default unit price in the
            company's currency and default uom of the product
        :param quantity: the quantity of product
        :param uom: the BrowseRecord of the product.uom
        :return: a dictionary
        '''
        res = super(PriceList, self)._get_context_price_list_line(
            party, product, unit_price, quantity, uom)
        res['party'] = party
        res['product'] = product
        res['quantity'] = quantity
        res['uom'] = uom
        res['price_list'] = Pool().get('product.price_list')
        return res

    @classmethod
    def compute_price_list(self, price_list):
        '''
        Compute price based on another price list

        :param price_list: the price list id or the BrowseRecord of the
            product.price_list
        :return: the computed unit price
        '''
        if isinstance(price_list, (int, long)):
            price_list = self(price_list)

        try:
            price_list.name
        except:
            self.raise_user_error('not_found_price_list', price_list)

        product = Transaction().context['product']
        return self.compute(
                        price_list,
                        Transaction().context['party'],
                        product,
                        Transaction().context['unit_price'],
                        Transaction().context.get('quantity', 0.0),
                        Transaction().context.get('uom', product.default_uom))


class PriceListLine:
    __name__ = 'product.price_list.line'

    @classmethod
    def __setup__(cls):
        super(PriceListLine, cls).__setup__()
        cls._error_messages.update({
                'add_product': ('The formula test requires to add at least '
                    'one product for a used item in this formula.'),
                })

    def check_formula(self):
        '''
        Check formula
        '''
        context = self._get_check_context()

        with Transaction().set_context(**context):
            try:
                if not isinstance(self.get_unit_price(), Decimal):
                    self.raise_user_error('invalid_formula', {
                            'formula': self.formula,
                            'line': self.rec_name,
                            })
            except Exception:
                self.raise_user_error('invalid_formula', {
                        'formula': self.formula,
                        'line': self.rec_name,
                        })

    def _get_check_context(self):
        '''
        Provide an extensible context for check_formula
        '''
        pool = Pool()
        Party = pool.get('party.party')
        Product = pool.get('product.product')
        PriceList = pool.get('product.price_list')
        party = None
        product = None
        uom = None
        items = ['product.', 'party.', 'quantity.', 'price_list.']
        for item in items:
            if item in self.formula:
                products = Product.search([], limit=1)
                if not products:
                    self.raise_user_error('add_product')
                product = products[0]
                party = Party.search([], limit=1)[0]
                uom = products[0].default_uom
                break
        context = PriceList()._get_context_price_list_line(party, product,
                Decimal('0.0'), 0, uom)
        return context
